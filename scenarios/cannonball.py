import math
import time

from require import require

logger = require('../util/logger')
sim = require('../sim')

"""
extremely dumb cannonball sim
"""

state = [
    0, # time s
    0, # x m
    0, # y m
    20, #, vx m/s
    30, # vy m/s
]
MARS_GRAVITY = 3.72076

def compute_derivatives(state):
    # pretend it takes a long time
    time.sleep(0.005)
    return [
        1,
        state[3],
        state[4],
        0, # assume ballistic coeeficient is crazy high on mars, no drag
        -1 * MARS_GRAVITY
    ]

def log(state):
    vmag = math.sqrt(state[3] * state[3]  +  state[4] * state[4])
    data = state + [vmag]
    logger.log(f'x: {data[1]}, y: {data[2]}')
    return data

def main():
    def detect_impact(state):
        y = state[2]
        vy = state[4]
        return y <= 0 and vy < 0

    data = sim.run(state, compute_derivatives, detect_impact, 0.01, log, 1)
    logger.log('done')

    print('\nexample data\n\nt, x, y, vx, vy, vmag')
    for dp in data:
        print(', '.join([str(x) for x in dp]))


if __name__ == '__main__':
    main()