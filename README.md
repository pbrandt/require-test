# Require for python

commonjs-like imports for python

```bash
pip install -e ./require
python scenarios/cannonball.py
```


```py
from require import require

datetools = require('./util/datetools.py')
```

In your python file, whatever is named `exports` is exported, whether it's a class, instance of a class, dict, or whatever.


## examples

`scenarios/cannonball.py` - a stupid cannonball sim on mars

`scenarios/marsDROP.py` - a tiny mars lander, (pdf)[https://digitalcommons.usu.edu/cgi/viewcontent.cgi?article=3237&context=smallsat]

`scenarios/covid.py` - simulate the covid-19 virus


