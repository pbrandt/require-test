import traceback
import os

from require import require

datetools = require('./datetools.py')

def log(string):
    sim_time = round(datetools.sim_time, 1)
    clock_time = datetools.get_sim_clock_time()
    clock_time = round(clock_time, 2)
    print(f'[SIM TIME: {sim_time}] [CLOCK TIME: {clock_time}] {string}')

def debug(string):
    if 'DEBUG' in os.environ:
        log(string)

class exports:
    log = log
    debug = debug
