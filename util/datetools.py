import time

class DateTools:
    # "Sim execution time" as the underlying time variable of the dynamics sim
    sim_time = 0

    # "Sim clock time" for performance metric / logging
    sim_start_clock_time = 0

    def __init__(self):
        self.sim_time = 0
        self.sim_start_clock_time = time.time()

    def reset_sim_clock_time(self):
        self.sim_start_clock_time = time.time()

    def set_sim_time(self, v):
        self.sim_time = v

    def increment_sim_time(self, v):
        self.sim_time = self.sim_time + v

    def get_sim_clock_time(self):
        """ returns time since the last call of reset_sim_time() in seconds """
        return time.time() - self.sim_start_clock_time

# export an instance of this class
exports = DateTools()
