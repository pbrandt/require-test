from require import require

logger = require('./logger.py')

def interpolate(table, x):
    """ interpolate stuff, table is a list of [x, y] pairs, and x is the desired x value to get the y of """
    logger.debug(f'interpolate x = {x}')
    low = table[0]
    high = table[-1]
    
    if x < low[0]:
        logger.log(f'Warning: interpolating out of bounds, giving lowest bound. was {x}')
        return low[1]
    elif x > high[0]:
        logger.log(f'Warning: interpolating out of bounds, giving highest bound. was {x}')
        return high[1]
    
    i = 1
    while table[i][0] < x:
        low = table[i]
        i = i+1
        high = table[i]
    
    return (high[1] - low[1]) / (high[0] - low[0]) * (x - low[0]) + low[1]

exports = interpolate