import os

from require import require

datetools = require('../util/datetools.py')
logger = require('../util/logger.py')

def run(state, derivative_function, end_condition, dt, log_function, log_interval):
    """ run a sim

    state: anything you wish to propagate.

    derivative_function: takes state as the only argument, computes derivatives. if you're derivatives are time-dependent, then include time in your state, with a derivative = 1

    end_condition: takes state as the only argument, tells the sim when to stop.

    log_function: takes state as the only argument, returns an array to log

    log_interval: how often to log in seconds
    """
    logger.log("NO you can't just use euler's method!!!!")
    logger.log("haha sim go brrrrr")

    data_log = []
    next_log_time = datetools.sim_time + log_interval

    # log first time
    data_log.append(log_function(state))
    
    while not end_condition(state):
        derivatives = derivative_function(state)
        
        for i, x in enumerate(state):
            state[i] = x + derivatives[i] * dt
        
        datetools.increment_sim_time(dt)
        
        if 'DEBUG' in os.environ:
            log_function(state)
        elif abs(datetools.sim_time - next_log_time) < dt/10:
            data_log.append(log_function(state))
            next_log_time = next_log_time + log_interval
    
    # log last time
    data_log.append(log_function(state))

    return data_log

class exports:
    run = run
