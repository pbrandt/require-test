from setuptools import setup

setup(
    name="require",
    version="1.0.0",
    description="relative imports like commonjs",
    py_modules=["require"]
)
