import importlib.util
import traceback
import os

__require__exports = {}

def debug(s):
    if 'DEBUG' in os.environ and os.environ['DEBUG'] in [1, '1', 'True', 'true', 'TRUE']:
        print(s)

def require(relative_path):
    """ perform a module load relative to local directory

    Description:
        Loads a python file at 'relative_path' as if it were a module,
        then returns the "exports" variable from the module.
    
    Example:
        pip install -e ./require


        from require import require
        dateconv = require('./util/dateconv')


    """
    # first get the calling code's local directory
    tb = traceback.extract_stack()
    calling_file = tb[-2].filename
    calling_file_dir = os.path.dirname(calling_file)

    # get the absolute path for the require'd code by joining the calling dir and the specified path
    relative_path = os.path.normpath(relative_path)
    required_file = os.path.abspath(os.path.join(calling_file_dir, relative_path))
    debug(f'requierd file: {required_file}')

    # handle require('./somefile.py'), require('./somedir') that has an index.py, and require('./somefile') where the .py is assumed
    if os.path.isfile(required_file):
        pass
    elif os.path.isdir(required_file) and os.path.isfile(os.path.join(required_file, 'index.py')):
        required_file = os.path.join(required_file, 'index.py')
    elif os.path.isfile(required_file + '.py'):
        required_file = required_file + '.py'

    # compile a "module name" for this
    module_name = '.'.join(required_file.split(os.sep)[1:])
    debug(f'module name: {module_name}')

    # check for cached version
    if module_name in __require__exports:
        return __require__exports[module_name]

    spec = importlib.util.spec_from_file_location(module_name, required_file)
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)

    # cache for later
    __require__exports[module_name] = mod.exports
    return mod.exports
